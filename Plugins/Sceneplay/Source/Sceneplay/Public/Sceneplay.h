#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Sceneplay.generated.h"

UCLASS(Abstract, Blueprintable, BlueprintType)
class SCENEPLAY_API USceneplay : public UObject
{
    GENERATED_BODY()

    friend class USceneplayComponent;

public:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sceneplay)
    FGameplayTagContainer GameplayTags;

    DECLARE_EVENT(USceneplay, FBeginSceneEvent)
    FBeginSceneEvent& OnBeginScene() { return BeginSceneEvent; }

    DECLARE_EVENT(USceneplay, FEndSceneEvent)
    FEndSceneEvent& OnEndScene() { return EndSceneEvent; }

protected:
    virtual bool CanBeginScene() const;

    UFUNCTION(BlueprintNativeEvent, Category = Sceneplay,
              meta = (ScriptName = "CanBeginScene", DisplayName = "Can Begin Scene"))
    bool Blueprint_CanBeginScene() const;

    virtual void BeginScene();

    UFUNCTION(BlueprintImplementableEvent, Category = Sceneplay,
              meta = (ScriptName = "BeginScene", DisplayName = "Begin Scene"))
    void Blueprint_BeginScene();

    UFUNCTION(BlueprintCallable, Category = Sceneplay)
    virtual void EndScene();

    UFUNCTION(BlueprintImplementableEvent, Category = Sceneplay,
              meta = (ScriptName = "OnEndScene", DisplayName = "On End Scene"))
    void Blueprint_OnEndScene();

private:
    FBeginSceneEvent BeginSceneEvent;
    FEndSceneEvent EndSceneEvent;
};