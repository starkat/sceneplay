#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "SceneplayComponent.generated.h"

UCLASS(Blueprintable, BlueprintType)
class USceneplayComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    USceneplayComponent();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sceneplay)
    uint8 bBeginSceneOnActivated : 1;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sceneplay)
    FGameplayTagContainer GameplayTags;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = Sceneplay)
    class USceneplay* Sceneplay;

    virtual void Activate(bool bReset) override;
};