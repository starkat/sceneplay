#pragma once

#include "CoreMinimal.h"
#include "Engine/Blueprint.h"
#include "SceneplayBlueprint.generated.h"

UCLASS(BlueprintType)
class SCENEPLAY_API USceneplayBlueprint : public UBlueprint
{
    GENERATED_BODY()

#if WITH_EDITOR
    // UBlueprint interface
    virtual bool SupportedByDefaultBlueprintFactory() const override { return false; }
    // End of UBlueprint interface
#endif
};