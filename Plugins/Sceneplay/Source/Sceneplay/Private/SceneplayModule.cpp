#include "SceneplayModule.h"
#include "SceneplayLog.h"

#define LOCTEXT_NAMESPACE "FSceneplayModule"

DEFINE_LOG_CATEGORY(LogSceneplay)

class FSceneplayModule : public ISceneplayModule
{
public:
    /** IModuleInterface implementation */
    virtual void StartupModule() override;
    virtual void ShutdownModule() override;
};

void FSceneplayModule::StartupModule()
{
    // This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin
    // file per-module
}

void FSceneplayModule::ShutdownModule()
{
    // This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
    // we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSceneplayModule, Sceneplay)