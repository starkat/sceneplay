#include "Sceneplay.h"
#include "SceneplayLog.h"

bool USceneplay::CanBeginScene() const
{
    bool CanBegin = Blueprint_CanBeginScene();
    UE_LOG(LogSceneplay, Verbose, TEXT("USceneplay::Blueprint_CanBeginScene: %s CanBegin result is %s"),
           *GetNameSafe(this), CanBegin ? TEXT("true") : TEXT("false"));
    return CanBegin;
}

bool USceneplay::Blueprint_CanBeginScene_Implementation() const
{
    return true;
}

void USceneplay::BeginScene()
{
    if (CanBeginScene())
    {
        UE_LOG(LogSceneplay, Display, TEXT("USceneplay::BeginScene: %s is beginning"), *GetNameSafe(this));
        BeginSceneEvent.Broadcast();
        Blueprint_BeginScene();
    }
    else
    {
        UE_LOG(LogSceneplay, Log, TEXT("USceneplay::BeginScene: %s did not begin"), *GetNameSafe(this));
    }
}

void USceneplay::EndScene()
{
    UE_LOG(LogSceneplay, Log, TEXT("USceneplay::EndScene: %s is ending"), *GetNameSafe(this));
    Blueprint_OnEndScene();
    EndSceneEvent.Broadcast();
    UE_LOG(LogSceneplay, Display, TEXT("USceneplay::EndScene: %s ended"), *GetNameSafe(this));
}