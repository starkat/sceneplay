#include "SceneplayComponent.h"
#include "Sceneplay.h"
#include "SceneplayLog.h"

USceneplayComponent::USceneplayComponent() : bBeginSceneOnActivated(false)
{
}

void USceneplayComponent::Activate(bool bReset)
{
    Super::Activate(bReset);

    if (bBeginSceneOnActivated && Sceneplay != nullptr)
    {
        UE_LOG(LogSceneplay, Log, TEXT("USceneplayComponent::Activate: %s beginning scene on activation"),
               *GetNameSafe(this));
        Sceneplay->BeginScene();
    }
}