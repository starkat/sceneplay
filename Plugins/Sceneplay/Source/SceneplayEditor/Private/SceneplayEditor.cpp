#include "SceneplayEditor.h"
#include "EditorReimportHandler.h"

#if WITH_EDITOR
#include "Editor.h"
#endif

#include "Kismet2/BlueprintEditorUtils.h"
#include "SceneplayBlueprint.h"
#include "SceneplayGraphSchema.h"

#define LOCTEXT_NAMESPACE "FSceneplayEditor"

/////////////////////////////////////////////////////
// FSceneplayEditor

FSceneplayEditor::FSceneplayEditor()
{
}

FSceneplayEditor::~FSceneplayEditor()
{
}

void FSceneplayEditor::InitSceneplayEditor(const EToolkitMode::Type Mode,
                                           const TSharedPtr<IToolkitHost>& InitToolkitHost,
                                           const TArray<UBlueprint*>& InBlueprints, bool bShouldOpenInDefaultsMode)
{
    InitBlueprintEditor(Mode, InitToolkitHost, InBlueprints, bShouldOpenInDefaultsMode);

    for (auto Blueprint : InBlueprints)
    {
        EnsureSceneplayBlueprintIsUpToDate(Blueprint);
    }
}

void FSceneplayEditor::EnsureSceneplayBlueprintIsUpToDate(UBlueprint* Blueprint)
{
#if WITH_EDITORONLY_DATA
    for (UEdGraph* Graph : Blueprint->UbergraphPages)
    {
        // remove the default event graph, if it exists, from existing Sceneplay Blueprints
        if (Graph->GetName() == "EventGraph" && Graph->Nodes.Num() == 0)
        {
            check(!Graph->Schema->GetClass()->IsChildOf(USceneplayGraphSchema::StaticClass()));
            FBlueprintEditorUtils::RemoveGraph(Blueprint, Graph);
            break;
        }
    }
#endif
}

FName FSceneplayEditor::GetToolkitFName() const
{
    return FName("SceneplayEditor");
}

FText FSceneplayEditor::GetBaseToolkitName() const
{
    return LOCTEXT("SceneplayEditorAppLabel", "Sceneplay Editor");
}

FText FSceneplayEditor::GetToolkitName() const
{
    const TArray<UObject*>& EditingObjs = GetEditingObjects();

    check(EditingObjs.Num() > 0);

    FFormatNamedArguments Args;

    const UObject* EditingObject = EditingObjs[0];

    const bool bDirtyState = EditingObject->GetOutermost()->IsDirty();

    Args.Add(TEXT("ObjectName"), FText::FromString(EditingObject->GetName()));
    Args.Add(TEXT("DirtyState"), bDirtyState ? FText::FromString(TEXT("*")) : FText::GetEmpty());
    return FText::Format(LOCTEXT("SceneplayToolkitName", "{ObjectName}{DirtyState}"), Args);
}

FText FSceneplayEditor::GetToolkitToolTipText() const
{
    const UObject* EditingObject = GetEditingObject();

    check(EditingObject != nullptr);

    return FAssetEditorToolkit::GetToolTipTextForObject(EditingObject);
}

FString FSceneplayEditor::GetWorldCentricTabPrefix() const
{
    return TEXT("SceneplayEditor");
}

FLinearColor FSceneplayEditor::GetWorldCentricTabColorScale() const
{
    return FLinearColor::White;
}

UBlueprint* FSceneplayEditor::GetBlueprintObj() const
{
    const TArray<UObject*>& EditingObjs = GetEditingObjects();
    for (int32 i = 0; i < EditingObjs.Num(); ++i)
    {
        if (EditingObjs[i]->IsA<USceneplayBlueprint>())
        {
            return (UBlueprint*)EditingObjs[i];
        }
    }
    return nullptr;
}

FString FSceneplayEditor::GetDocumentationLink() const
{
    return FBlueprintEditor::GetDocumentationLink(); // todo: point this at the correct documentation
}

#undef LOCTEXT_NAMESPACE
