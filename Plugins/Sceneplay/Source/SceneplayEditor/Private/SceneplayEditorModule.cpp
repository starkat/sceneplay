#include "SceneplayEditorModule.h"
#include "AssetTypeActions_SceneplayBlueprint.h"
#include "AssetTypeCategories.h"
#include "IAssetTools.h"
#include "IAssetTypeActions.h"
#include "SceneplayEditorLog.h"

#define LOCTEXT_NAMESPACE "FSceneplayEditorModule"

DEFINE_LOG_CATEGORY(LogSceneplayEditor)

class FSceneplayEditorModule : public ISceneplayEditorModule
{
public:
    /** IModuleInterface implementation */
    virtual void StartupModule() override;
    virtual void ShutdownModule() override;

private:
    void RegisterAssetTypeAction(IAssetTools& AssetTools, TSharedRef<IAssetTypeActions> Action);

private:
    TArray<TSharedPtr<IAssetTypeActions>> CreatedAssetTypeActions;

    EAssetTypeCategories::Type SceneplayAssetCategoryBit;
};

void FSceneplayEditorModule::StartupModule()
{
    IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
    SceneplayAssetCategoryBit = AssetTools.RegisterAdvancedAssetCategory(
        FName(TEXT("Sceneplay")), LOCTEXT("SceneplayAssetCategory", "Sceneplay"));

    RegisterAssetTypeAction(AssetTools,
                            MakeShareable(new FAssetTypeActions_SceneplayBlueprint(SceneplayAssetCategoryBit)));
}

void FSceneplayEditorModule::ShutdownModule()
{
    // Unregister all the asset types that we registered
    if (FModuleManager::Get().IsModuleLoaded("AssetTools"))
    {
        IAssetTools& AssetTools = FModuleManager::GetModuleChecked<FAssetToolsModule>("AssetTools").Get();
        for (int32 Index = 0; Index < CreatedAssetTypeActions.Num(); ++Index)
        {
            AssetTools.UnregisterAssetTypeActions(CreatedAssetTypeActions[Index].ToSharedRef());
        }
    }
}

void FSceneplayEditorModule::RegisterAssetTypeAction(IAssetTools& AssetTools, TSharedRef<IAssetTypeActions> Action)
{
    AssetTools.RegisterAssetTypeActions(Action);
    CreatedAssetTypeActions.Add(Action);
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FSceneplayEditorModule, SceneplayEditor)