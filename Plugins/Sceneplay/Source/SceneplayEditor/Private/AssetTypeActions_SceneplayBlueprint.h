#pragma once

#include "AssetTypeActions/AssetTypeActions_Blueprint.h"
#include "CoreMinimal.h"
#include "Toolkits/IToolkitHost.h"

class UFactory;

class FAssetTypeActions_SceneplayBlueprint : public FAssetTypeActions_Blueprint
{
public:
    FAssetTypeActions_SceneplayBlueprint(EAssetTypeCategories::Type InAssetCategory);

    // IAssetTypeActions Implementation
    virtual FText GetName() const override;
    virtual FColor GetTypeColor() const override;
    virtual UClass* GetSupportedClass() const override;
    virtual void OpenAssetEditor(
        const TArray<UObject*>& InObjects,
        TSharedPtr<class IToolkitHost> EditWithinLevelEditor = TSharedPtr<IToolkitHost>()) override;
    virtual uint32 GetCategories() override { return EAssetTypeCategories::Blueprint | AssetCategory; }
    // End IAssetTypeActions Implementation

    // FAssetTypeActions_Blueprint interface
    virtual UFactory* GetFactoryForBlueprintType(UBlueprint* InBlueprint) const override;

private:
    /** Returns true if the blueprint is data only */
    bool ShouldUseDataOnlyEditor(const UBlueprint* Blueprint) const;

private:
    EAssetTypeCategories::Type AssetCategory;
};
