// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AssetTypeActions_SceneplayBlueprint.h"
#include "Kismet2/BlueprintEditorUtils.h"
#include "Misc/MessageDialog.h"
#include "Sceneplay.h"
#include "SceneplayBlueprint.h"
#include "SceneplayBlueprintFactory.h"
#include "SceneplayEditor.h"

#define LOCTEXT_NAMESPACE "AssetTypeActions"

FAssetTypeActions_SceneplayBlueprint::FAssetTypeActions_SceneplayBlueprint(EAssetTypeCategories::Type InAssetCategory)
    : AssetCategory(InAssetCategory)
{
}

FText FAssetTypeActions_SceneplayBlueprint::GetName() const
{
    return LOCTEXT("AssetTypeActions_SceneplayBlueprint", "Sceneplay Blueprint");
}

FColor FAssetTypeActions_SceneplayBlueprint::GetTypeColor() const
{
    return FColor(0, 96, 128);
}

void FAssetTypeActions_SceneplayBlueprint::OpenAssetEditor(const TArray<UObject*>& InObjects,
                                                           TSharedPtr<IToolkitHost> EditWithinLevelEditor)
{
    EToolkitMode::Type Mode = EditWithinLevelEditor.IsValid() ? EToolkitMode::WorldCentric : EToolkitMode::Standalone;

    for (auto ObjIt = InObjects.CreateConstIterator(); ObjIt; ++ObjIt)
    {
        auto Blueprint = Cast<UBlueprint>(*ObjIt);
        if (Blueprint)
        {
            bool bLetOpen = true;
            if (!Blueprint->ParentClass)
            {
                bLetOpen = EAppReturnType::Yes ==
                           FMessageDialog::Open(
                               EAppMsgType::YesNo,
                               LOCTEXT("FailedToLoadSceneplayBlueprintWithContinue",
                                       "Sceneplay Blueprint could not be loaded because it derives from an "
                                       "invalid class.  Check to make sure the parent class for this blueprint hasn't "
                                       "been removed! Do you want to continue (it can crash the editor)?"));
            }

            if (bLetOpen)
            {
                TSharedRef<FSceneplayEditor> NewEditor(new FSceneplayEditor());

                TArray<UBlueprint*> Blueprints;
                Blueprints.Add(Blueprint);

                NewEditor->InitSceneplayEditor(Mode, EditWithinLevelEditor, Blueprints,
                                               ShouldUseDataOnlyEditor(Blueprint));
            }
        }
        else
        {
            FMessageDialog::Open(
                EAppMsgType::Ok,
                LOCTEXT("FailedToLoadSceneplayBlueprint",
                        "Sceneplay Blueprint could not be loaded because it derives from an invalid class.  "
                        "Check to make sure the parent class for this blueprint hasn't been removed!"));
        }
    }
}

bool FAssetTypeActions_SceneplayBlueprint::ShouldUseDataOnlyEditor(const UBlueprint* Blueprint) const
{
    return FBlueprintEditorUtils::IsDataOnlyBlueprint(Blueprint) &&
           !FBlueprintEditorUtils::IsLevelScriptBlueprint(Blueprint) &&
           !FBlueprintEditorUtils::IsInterfaceBlueprint(Blueprint) && !Blueprint->bForceFullEditor &&
           !Blueprint->bIsNewlyCreated;
}

UClass* FAssetTypeActions_SceneplayBlueprint::GetSupportedClass() const
{
    return USceneplayBlueprint::StaticClass();
}

UFactory* FAssetTypeActions_SceneplayBlueprint::GetFactoryForBlueprintType(UBlueprint* InBlueprint) const
{
    USceneplayBlueprintFactory* SceneplayBlueprintFactory = NewObject<USceneplayBlueprintFactory>();
    SceneplayBlueprintFactory->ParentClass = TSubclassOf<USceneplay>(*InBlueprint->GeneratedClass);
    return SceneplayBlueprintFactory;
}

#undef LOCTEXT_NAMESPACE
