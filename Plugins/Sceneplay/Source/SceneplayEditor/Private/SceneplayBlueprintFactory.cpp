#include "SceneplayBlueprintFactory.h"
#include "EdGraphSchema_K2.h"
#include "Editor.h"
#include "EditorStyleSet.h"
#include "Input/Reply.h"
#include "InputCoreTypes.h"
#include "Layout/Visibility.h"
#include "Misc/MessageDialog.h"
#include "Modules/ModuleManager.h"
#include "Sceneplay.h"
#include "UObject/Interface.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Layout/SBorder.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Layout/SUniformGridPanel.h"
#include "Widgets/SBoxPanel.h"
#include "Widgets/SCompoundWidget.h"
#include "Widgets/SWidget.h"
#include "Widgets/SWindow.h"
#include "Widgets/Text/STextBlock.h"

#include "BlueprintEditorSettings.h"
#include "ClassViewerModule.h"
#include "Engine/BlueprintGeneratedClass.h"
#include "Kismet2/BlueprintEditorUtils.h"
#include "Kismet2/KismetEditorUtilities.h"

#include "SceneplayBlueprint.h"
#include "SceneplayGraph.h"
#include "SceneplayGraphSchema.h"

#include "ClassViewerFilter.h"

#include "SlateOptMacros.h"

#define LOCTEXT_NAMESPACE "USceneplayBlueprintFactory"

// ------------------------------------------------------------------------------
// Dialog to configure creation properties
// ------------------------------------------------------------------------------
BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION

class SSceneplayBlueprintCreateDialog : public SCompoundWidget
{
public:
    SLATE_BEGIN_ARGS(SSceneplayBlueprintCreateDialog) {}

    SLATE_END_ARGS()

    /** Constructs this widget with InArgs */
    void Construct(const FArguments& InArgs)
    {
        bOkClicked = false;
        ParentClass = USceneplay::StaticClass();

        ChildSlot
            [SNew(SBorder)
                 .Visibility(EVisibility::Visible)
                 .BorderImage(FEditorStyle::GetBrush("Menu.Background"))
                     [SNew(SBox)
                          .Visibility(EVisibility::Visible)
                          .WidthOverride(500.0f)
                              [SNew(SVerticalBox) +
                               SVerticalBox::Slot().FillHeight(
                                   1)[SNew(SBorder)
                                          .BorderImage(FEditorStyle::GetBrush("ToolPanel.GroupBorder"))
                                          .Content()[SAssignNew(ParentClassContainer, SVerticalBox)]]

                               // Ok/Cancel buttons
                               + SVerticalBox::Slot()
                                     .AutoHeight()
                                     .HAlign(HAlign_Right)
                                     .VAlign(VAlign_Bottom)
                                     .Padding(
                                         8)[SNew(SUniformGridPanel)
                                                .SlotPadding(FEditorStyle::GetMargin("StandardDialog.SlotPadding"))
                                                .MinDesiredSlotWidth(
                                                    FEditorStyle::GetFloat("StandardDialog.MinDesiredSlotWidth"))
                                                .MinDesiredSlotHeight(
                                                    FEditorStyle::GetFloat("StandardDialog.MinDesiredSlotHeight")) +
                                            SUniformGridPanel::Slot(0, 0)
                                                [SNew(SButton)
                                                     .HAlign(HAlign_Center)
                                                     .ContentPadding(FEditorStyle::GetMargin("StandardDialog."
                                                                                             "ContentPadding"))
                                                     .OnClicked(this, &SSceneplayBlueprintCreateDialog::OkClicked)
                                                     .Text(LOCTEXT("CreateSceneplayBlueprintOk", "OK"))] +
                                            SUniformGridPanel::Slot(1, 0)
                                                [SNew(SButton)
                                                     .HAlign(HAlign_Center)
                                                     .ContentPadding(FEditorStyle::GetMargin("StandardDialog."
                                                                                             "ContentPadding"))
                                                     .OnClicked(this, &SSceneplayBlueprintCreateDialog::CancelClicked)
                                                     .Text(LOCTEXT("CreateSceneplayBlueprintCancel", "Cancel"))]]]]];

        MakeParentClassPicker();
    }

    /** Sets properties for the supplied SceneplayBlueprintFactory */
    bool ConfigureProperties(TWeakObjectPtr<USceneplayBlueprintFactory> InSceneplayBlueprintFactory)
    {
        SceneplayBlueprintFactory = InSceneplayBlueprintFactory;

        TSharedRef<SWindow> Window =
            SNew(SWindow)
                .Title(LOCTEXT("CreateSceneplayBlueprintOptions", "Create Sceneplay Blueprint"))
                .ClientSize(FVector2D(400, 700))
                .SupportsMinimize(false)
                .SupportsMaximize(false)[AsShared()];

        PickerWindow = Window;

        GEditor->EditorAddModalWindow(Window);
        SceneplayBlueprintFactory.Reset();

        return bOkClicked;
    }

private:
    class FSceneplayBlueprintParentFilter : public IClassViewerFilter
    {
    public:
        /** All children of these classes will be included unless filtered out by another setting. */
        TSet<const UClass*> AllowedChildrenOfClasses;

        FSceneplayBlueprintParentFilter() {}

        virtual bool IsClassAllowed(const FClassViewerInitializationOptions& InInitOptions, const UClass* InClass,
                                    TSharedRef<FClassViewerFilterFuncs> InFilterFuncs) override
        {
            // If it appears on the allowed child-of classes list (or there is nothing on that list)
            return InFilterFuncs->IfInChildOfClassesSet(AllowedChildrenOfClasses, InClass) != EFilterReturn::Failed;
        }

        virtual bool IsUnloadedClassAllowed(const FClassViewerInitializationOptions& InInitOptions,
                                            const TSharedRef<const IUnloadedBlueprintData> InUnloadedClassData,
                                            TSharedRef<FClassViewerFilterFuncs> InFilterFuncs) override
        {
            // If it appears on the allowed child-of classes list (or there is nothing on that list)
            return InFilterFuncs->IfInChildOfClassesSet(AllowedChildrenOfClasses, InUnloadedClassData) !=
                   EFilterReturn::Failed;
        }
    };

    /** Creates the combo menu for the parent class */
    void MakeParentClassPicker()
    {
        // Load the classviewer module to display a class picker
        FClassViewerModule& ClassViewerModule = FModuleManager::LoadModuleChecked<FClassViewerModule>("ClassViewer");

        // Fill in options
        FClassViewerInitializationOptions Options;
        Options.Mode = EClassViewerMode::ClassPicker;

        // Only allow parenting to base blueprints.
        Options.bIsBlueprintBaseOnly = true;

        TSharedPtr<FSceneplayBlueprintParentFilter> Filter = MakeShareable(new FSceneplayBlueprintParentFilter);

        // All child classes of USceneplay are valid.
        Filter->AllowedChildrenOfClasses.Add(USceneplay::StaticClass());
        Options.ClassFilter = Filter;

        ParentClassContainer->ClearChildren();
        ParentClassContainer->AddSlot().AutoHeight()
            [SNew(STextBlock).Text(LOCTEXT("ParentClass", "Parent Class:")).ShadowOffset(FVector2D(1.0f, 1.0f))];

        ParentClassContainer->AddSlot()[ClassViewerModule.CreateClassViewer(
            Options, FOnClassPicked::CreateSP(this, &SSceneplayBlueprintCreateDialog::OnClassPicked))];
    }

    /** Handler for when a parent class is selected */
    void OnClassPicked(UClass* ChosenClass) { ParentClass = ChosenClass; }

    /** Handler for when ok is clicked */
    FReply OkClicked()
    {
        if (SceneplayBlueprintFactory.IsValid())
        {
            SceneplayBlueprintFactory->BlueprintType = BPTYPE_Normal;
            SceneplayBlueprintFactory->ParentClass = ParentClass.Get();
        }

        CloseDialog(true);

        return FReply::Handled();
    }

    void CloseDialog(bool bWasPicked = false)
    {
        bOkClicked = bWasPicked;
        if (PickerWindow.IsValid())
        {
            PickerWindow.Pin()->RequestDestroyWindow();
        }
    }

    /** Handler for when cancel is clicked */
    FReply CancelClicked()
    {
        CloseDialog();
        return FReply::Handled();
    }

    FReply OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent)
    {
        if (InKeyEvent.GetKey() == EKeys::Escape)
        {
            CloseDialog();
            return FReply::Handled();
        }
        return SWidget::OnKeyDown(MyGeometry, InKeyEvent);
    }

private:
    /** The factory for which we are setting up properties */
    TWeakObjectPtr<USceneplayBlueprintFactory> SceneplayBlueprintFactory;

    /** A pointer to the window that is asking the user to select a parent class */
    TWeakPtr<SWindow> PickerWindow;

    /** The container for the Parent Class picker */
    TSharedPtr<SVerticalBox> ParentClassContainer;

    /** The selected class */
    TWeakObjectPtr<UClass> ParentClass;

    /** True if Ok was clicked */
    bool bOkClicked;
};

END_SLATE_FUNCTION_BUILD_OPTIMIZATION

/*------------------------------------------------------------------------------
    USceneplayBlueprintFactory implementation.
------------------------------------------------------------------------------*/

USceneplayBlueprintFactory::USceneplayBlueprintFactory()
{
    bCreateNew = true;
    bEditAfterNew = true;
    SupportedClass = USceneplayBlueprint::StaticClass();
    ParentClass = USceneplay::StaticClass();
}

bool USceneplayBlueprintFactory::ConfigureProperties()
{
    TSharedRef<SSceneplayBlueprintCreateDialog> Dialog = SNew(SSceneplayBlueprintCreateDialog);
    return Dialog->ConfigureProperties(this);
};

UObject* USceneplayBlueprintFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags,
                                                      UObject* Context, FFeedbackContext* Warn, FName CallingContext)
{
    // Make sure we are trying to factory a sceneplay blueprint, then create and init one
    check(Class->IsChildOf(USceneplayBlueprint::StaticClass()));

    // If they selected an interface, force the parent class to be UInterface
    if (BlueprintType == BPTYPE_Interface)
    {
        ParentClass = UInterface::StaticClass();
    }

    if ((ParentClass == nullptr) || !FKismetEditorUtilities::CanCreateBlueprintOfClass(ParentClass) ||
        !ParentClass->IsChildOf(USceneplay::StaticClass()))
    {
        FFormatNamedArguments Args;
        Args.Add(TEXT("ClassName"),
                 (ParentClass != nullptr) ? FText::FromString(ParentClass->GetName()) : LOCTEXT("Null", "(null)"));
        FMessageDialog::Open(
            EAppMsgType::Ok,
            FText::Format(LOCTEXT("CannotCreateSceneplayBlueprint",
                                  "Cannot create a Sceneplay Blueprint based on the class '{ClassName}'."),
                          Args));
        return nullptr;
    }
    else
    {
        USceneplayBlueprint* NewBP = CastChecked<USceneplayBlueprint>(FKismetEditorUtilities::CreateBlueprint(
            ParentClass, InParent, Name, BlueprintType, USceneplayBlueprint::StaticClass(),
            UBlueprintGeneratedClass::StaticClass(), CallingContext));

        if (NewBP)
        {
            const UEdGraphSchema_K2* K2Schema = GetDefault<UEdGraphSchema_K2>();

            // Only allow a sceneplay graph if there isn't one in a parent blueprint
            UEdGraph* NewGraph = FBlueprintEditorUtils::CreateNewGraph(
                NewBP, TEXT("Sceneplay Graph"), USceneplayGraph::StaticClass(), USceneplayGraphSchema::StaticClass());
#if WITH_EDITORONLY_DATA
            if (NewBP->UbergraphPages.Num())
            {
                FBlueprintEditorUtils::RemoveGraphs(NewBP, NewBP->UbergraphPages);
            }
#endif
            FBlueprintEditorUtils::AddUbergraphPage(NewBP, NewGraph);
            NewBP->LastEditedDocuments.Add(NewGraph);
            NewGraph->bAllowDeletion = false;

            UBlueprintEditorSettings* Settings = GetMutableDefault<UBlueprintEditorSettings>();
            if (Settings && Settings->bSpawnDefaultBlueprintNodes)
            {
                int32 NodePositionY = 0;
                FKismetEditorUtilities::AddDefaultEventNode(NewBP, NewGraph, FName(TEXT("Blueprint_BeginScene")),
                                                            USceneplay::StaticClass(), NodePositionY);
            }
        }

        return NewBP;
    }
}

UObject* USceneplayBlueprintFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags,
                                                      UObject* Context, FFeedbackContext* Warn)
{
    return FactoryCreateNew(Class, InParent, Name, Flags, Context, Warn, NAME_None);
}

#undef LOCTEXT_NAMESPACE
