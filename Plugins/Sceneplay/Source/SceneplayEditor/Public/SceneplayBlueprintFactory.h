#pragma once

#include "CoreMinimal.h"
#include "Engine/Blueprint.h"
#include "Factories/Factory.h"
#include "SceneplayBlueprintFactory.generated.h"

UCLASS(HideCategories = Object, MinimalAPI)
class USceneplayBlueprintFactory : public UFactory
{
    GENERATED_BODY()

public:
    USceneplayBlueprintFactory();

    // The type of blueprint that will be created
    UPROPERTY(EditAnywhere, Category = SceneplayBlueprintFactory)
    TEnumAsByte<enum EBlueprintType> BlueprintType;

    // The parent class of the created blueprint
    UPROPERTY(EditAnywhere, Category = SceneplayBlueprintFactory)
    TSubclassOf<class USceneplay> ParentClass;

    //~ Begin UFactory Interface
    virtual bool ConfigureProperties() override;
    virtual UObject* FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags,
                                      UObject* Context, FFeedbackContext* Warn, FName CallingContext) override;
    virtual UObject* FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags,
                                      UObject* Context, FFeedbackContext* Warn) override;
    //~ Begin UFactory Interface
};
