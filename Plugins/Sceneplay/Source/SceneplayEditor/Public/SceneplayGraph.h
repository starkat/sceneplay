#pragma once

#include "CoreMinimal.h"
#include "EdGraph/EdGraph.h"
#include "SceneplayGraph.generated.h"

UCLASS(MinimalAPI)
class USceneplayGraph : public UEdGraph
{
    GENERATED_BODY()
};
