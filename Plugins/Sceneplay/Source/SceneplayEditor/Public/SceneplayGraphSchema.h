#pragma once

#include "CoreMinimal.h"
#include "EdGraphSchema_K2.h"
#include "SceneplayGraphSchema.generated.h"

UCLASS(MinimalAPI)
class USceneplayGraphSchema : public UEdGraphSchema_K2
{
    GENERATED_BODY()
};
