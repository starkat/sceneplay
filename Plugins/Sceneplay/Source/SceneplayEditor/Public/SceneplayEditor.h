#pragma once

#include "CoreMinimal.h"
#include "Editor/Kismet/Public/BlueprintEditor.h"

//////////////////////////////////////////////////////////////////////////
// FSceneplayEditor

/**
 * Sceneplay asset editor (extends Blueprint editor)
 */
class FSceneplayEditor : public FBlueprintEditor
{
public:
    /**
     * Edits the specified sceneplay asset(s)
     *
     * @param	Mode					Asset editing mode for this editor (standalone or world-centric)
     * @param	InitToolkitHost			When Mode is WorldCentric, this is the level editor instance to spawn this
     * editor within
     * @param	InBlueprints			The blueprints to edit
     * @param	bShouldOpenInDefaultsMode	If true, the editor will open in defaults editing mode
     */

    void InitSceneplayEditor(const EToolkitMode::Type Mode, const TSharedPtr<IToolkitHost>& InitToolkitHost,
                             const TArray<UBlueprint*>& InBlueprints, bool bShouldOpenInDefaultsMode);

private:
    /**
     * Updates existing sceneplay blueprints to make sure that they are up to date
     *
     * @param	Blueprint	The blueprint to be updated
     */
    void EnsureSceneplayBlueprintIsUpToDate(UBlueprint* Blueprint);

public:
    FSceneplayEditor();

    virtual ~FSceneplayEditor();

public:
    // IToolkit interface
    virtual FName GetToolkitFName() const override;
    virtual FText GetBaseToolkitName() const override;
    virtual FText GetToolkitName() const override;
    virtual FText GetToolkitToolTipText() const override;
    virtual FString GetWorldCentricTabPrefix() const override;
    virtual FLinearColor GetWorldCentricTabColorScale() const override;
    // End of IToolkit interface

    /** @return the documentation location for this editor */
    virtual FString GetDocumentationLink() const override;

    /** Returns a pointer to the Blueprint object we are currently editing, as long as we are editing exactly one */
    virtual UBlueprint* GetBlueprintObj() const override;
};
