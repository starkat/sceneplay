// Fill out your copyright notice in the Description page of Project Settings.

using System.Collections.Generic;
using UnrealBuildTool;

public class SceneplayDemoEditorTarget : TargetRules
{
	public SceneplayDemoEditorTarget (TargetInfo Target) : base (Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange (new string[] { "SceneplayDemo" });
	}
}