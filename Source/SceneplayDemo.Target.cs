// Fill out your copyright notice in the Description page of Project Settings.

using System.Collections.Generic;
using UnrealBuildTool;

public class SceneplayDemoTarget : TargetRules
{
	public SceneplayDemoTarget (TargetInfo Target) : base (Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange (new string[] { "SceneplayDemo" });
	}
}